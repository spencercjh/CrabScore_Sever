package servlet.staff_part;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DataBase;

/**
 * Servlet implementation class DeleteCrabInfo
 */
@WebServlet("/DeleteCrabInfo")
public class DeleteCrabInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteCrabInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String str_crab_id = request.getParameter("crab_id");
		int crab_id = Integer.parseInt(str_crab_id);
		System.out.println("螃蟹id：	" + crab_id);
		PrintWriter out = response.getWriter();
		String delete_sql = "DELETE FROM `rxpb_crab_info` WHERE crab_id=" + crab_id;
		try {
			// 连接数据库
			java.sql.Connection conn = DriverManager.getConnection(DataBase.JDBC, DataBase.database_user_id,
					DataBase.database_user_password);
			Statement statement = conn.createStatement(); // 创建Statement对象
			// 执行SQL语句
			int result = statement.executeUpdate(delete_sql);
			if (result <= 0) {
				throw new SQLException();
			} else {
				System.out.println("delete crabinfo success");
				out.println("delete crabinfo success");
			}
			// 关闭连接
			conn.close();
			statement.close();
		} catch (SQLException se) {
			System.out.println("delete crabinfo failed");
			out.println("delete crabinfo failed");
			System.out.println("SQLException: " + se.getMessage());
		}
	}
}