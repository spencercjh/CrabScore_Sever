delimiter $$

CREATE TABLE `rxpb_company_info` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(45) NOT NULL,
  `competition_id` int(11) NOT NULL COMMENT '参赛企业所属的比赛,同一企业在不同的赛事中保持独立',
  `create_date` datetime DEFAULT NULL COMMENT '数据创建日期',
  `create_user` varchar(45) DEFAULT NULL COMMENT '数据创建用户',
  `update_date` datetime DEFAULT NULL COMMENT '数据更新日期',
  `update_user` varchar(45) DEFAULT NULL COMMENT '数据更新用户',
  PRIMARY KEY (`company_id`),
  UNIQUE KEY `company_name_UNIQUE` (`company_name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='企业信息表'$$

delimiter $$

CREATE TABLE `rxpb_competition_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自动,正常情况下始终为一条\n',
  `competition_id` int(11) NOT NULL COMMENT '此处设置的是当前正在进行的比赛\n',
  `create_date` datetime DEFAULT NULL COMMENT '数据创建日期',
  `create_user` varchar(45) DEFAULT NULL COMMENT '数据创建用户',
  `update_date` datetime DEFAULT NULL COMMENT '数据更新日期',
  `update_user` varchar(45) DEFAULT NULL COMMENT '数据更新用户',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='当前大赛配置\n正常情况下始终为一条\n'$$

delimiter $$

CREATE TABLE `rxpb_competition_info` (
  `competition_id` int(11) NOT NULL AUTO_INCREMENT,
  `competition_year` varchar(45) NOT NULL COMMENT '赛事的年份\nxxxx',
  `var_fatness_m` float NOT NULL COMMENT '雄蟹肥满度参数',
  `var_fatness_f` float NOT NULL COMMENT '雌蟹肥满度参数',
  `var_weight_m` float NOT NULL COMMENT '雄蟹体重参数',
  `var_weight_f` float NOT NULL COMMENT '雌蟹体重参数',
  `var_mfatness_sd` float NOT NULL COMMENT '雄蟹肥满度标准差参数',
  `var_mweight_sd` float NOT NULL COMMENT '雄蟹体重参数',
  `var_ffatness_sd` float NOT NULL COMMENT '雌蟹肥满度标准差参数',
  `var_fweight_sd` float NOT NULL COMMENT '雌蟹体重标准差参数',
  `result_fatness` int(11) NOT NULL DEFAULT '0' COMMENT '1:允许查看排名,0不允许查看排名',
  `result_quality` int(11) NOT NULL DEFAULT '0' COMMENT '1:允许查看排名,0不允许查看排名',
  `result_taste` int(11) NOT NULL DEFAULT '0' COMMENT '1:允许查看排名,0不允许查看排名',
  `note` varchar(225) DEFAULT NULL COMMENT '注备',
  `status` int(11) NOT NULL COMMENT '1：可用 0：禁用',
  `create_date` datetime DEFAULT NULL COMMENT '数据创建日期',
  `create_user` varchar(45) DEFAULT NULL COMMENT '数据创建用户',
  `update_date` datetime DEFAULT NULL COMMENT '数据更新日期',
  `update_user` varchar(45) DEFAULT NULL COMMENT '数据更新用户',
  PRIMARY KEY (`competition_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='大赛信息表'$$

delimiter $$

CREATE TABLE `rxpb_crab_info` (
  `crab_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `crab_sex` int(1) NOT NULL COMMENT '0:雌 1：雄',
  `crab_label` varchar(10) NOT NULL COMMENT '四位的蟹标识',
  `crab_weight` float DEFAULT NULL COMMENT '体重',
  `crab_length` float DEFAULT NULL COMMENT '壳长',
  `crab_fatness` float DEFAULT NULL COMMENT '肥满度',
  `create_date` datetime DEFAULT NULL COMMENT '数据创建日期',
  `create_user` varchar(45) DEFAULT NULL COMMENT '数据创建用户',
  `update_date` datetime DEFAULT NULL COMMENT '数据更新日期',
  `update_user` varchar(45) DEFAULT NULL COMMENT '数据更新用户',
  `competition_id` int(11) NOT NULL COMMENT '比赛ID',
  PRIMARY KEY (`crab_id`)
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8 COMMENT='蟹基本信息表,肥满度表'$$

delimiter $$

CREATE TABLE `rxpb_group_info` (
  `group_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '组ID',
  `company_id` int(11) NOT NULL,
  `competition_id` int(11) NOT NULL COMMENT '比赛ID',
  `fatness_score_m` float DEFAULT NULL COMMENT '雄蟹肥满度评分',
  `quality_score_m` float DEFAULT NULL COMMENT '雄蟹种质评分',
  `taste_score_m` float DEFAULT NULL COMMENT '雄蟹口感评分\n',
  `fatness_score_f` float DEFAULT NULL COMMENT '雌蟹肥满度评分\n',
  `quality_score_f` float DEFAULT NULL COMMENT '雌蟹种质评分\n',
  `taste_score_f` float DEFAULT NULL COMMENT '雌蟹口感评分\n',
  `create_date` datetime DEFAULT NULL COMMENT '数据创建日期',
  `create_user` varchar(45) DEFAULT NULL COMMENT '数据创建用户',
  `update_date` datetime DEFAULT NULL COMMENT '数据更新日期',
  `update_user` varchar(45) DEFAULT NULL COMMENT '数据更新用户',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='组信息数表'$$

delimiter $$

CREATE TABLE `rxpb_role_info` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '"角色ID:1：管理员\n 2：评委 3：工作人员 4:参选单位"\n',
  `role_name` varchar(15) NOT NULL,
  `create_date` datetime DEFAULT NULL COMMENT '数据创建日期',
  `create_user` varchar(45) DEFAULT NULL COMMENT '数据创建用户',
  `update_date` datetime DEFAULT NULL COMMENT '数据更新日期',
  `update_user` varchar(45) DEFAULT NULL COMMENT '数据更新用户',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8$$

delimiter $$

CREATE TABLE `rxpb_score_quality` (
  `score_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `crab_sex` int(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `score_fin` float DEFAULT NULL COMMENT '最终给分',
  `score_bts` float DEFAULT NULL COMMENT '体色(背)',
  `score_fts` float DEFAULT NULL COMMENT '体色(腹)',
  `score_ec` float DEFAULT NULL COMMENT '额齿',
  `score_dscc` float DEFAULT NULL COMMENT '第4侧齿',
  `score_bbyzt` float DEFAULT NULL COMMENT '背部疣状突',
  `create_date` datetime DEFAULT NULL COMMENT '数据创建日期',
  `create_user` varchar(45) DEFAULT NULL COMMENT '数据创建用户',
  `update_date` datetime DEFAULT NULL COMMENT '数据更新日期',
  `update_user` varchar(45) DEFAULT NULL COMMENT '数据更新用户',
  `competition_id` int(11) NOT NULL COMMENT '比赛ID',
  PRIMARY KEY (`score_id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COMMENT='种质评分表'$$

delimiter $$

CREATE TABLE `rxpb_score_taste` (
  `score_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `crab_sex` int(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `score_fin` float DEFAULT NULL COMMENT '最终给分',
  `score_ygys` float DEFAULT NULL COMMENT '蟹盖颜色',
  `score_sys` float DEFAULT NULL COMMENT '鳃颜色',
  `score_ghys` float DEFAULT NULL COMMENT '膏、黄颜色',
  `score_xwxw` float DEFAULT NULL COMMENT '腥味、香味',
  `score_gh` float DEFAULT NULL COMMENT '膏、黄',
  `score_fbjr` float DEFAULT NULL COMMENT '腹部肌肉',
  `score_bzjr` float DEFAULT NULL COMMENT '第二、三步足肌肉',
  `create_date` datetime DEFAULT NULL COMMENT '数据创建日期',
  `create_user` varchar(45) DEFAULT NULL COMMENT '数据创建用户',
  `update_date` datetime DEFAULT NULL COMMENT '数据更新日期',
  `update_user` varchar(45) DEFAULT NULL COMMENT '数据更新用户',
  `competition_id` int(11) NOT NULL COMMENT '比赛ID',
  PRIMARY KEY (`score_id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COMMENT='口感奖评分表'$$

delimiter $$

CREATE TABLE `rxpb_user_info` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) NOT NULL COMMENT '用户名,登录名',
  `password` varchar(45) NOT NULL COMMENT '密码',
  `display_name` varchar(45) NOT NULL DEFAULT '未命名用户' COMMENT '显示名称，姓名或单位名',
  `role_id` int(11) NOT NULL COMMENT '角色ID:1-管理员 2-工作人员 3-企业',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '用户状态 1：可用 0：禁用',
  `email` varchar(45) DEFAULT NULL COMMENT '''赛事信息（为0时代表永久有效）\r\n',
  `competition_id` int(11) NOT NULL,
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(45) DEFAULT NULL COMMENT '创建用户',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `update_user` varchar(45) DEFAULT NULL COMMENT '更新用户',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='用户信息表'$$

delimiter $$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `rxpb_score_fin` AS select `g`.`group_id` AS `group_id`,`c`.`company_name` AS `company_name`,`g`.`competition_id` AS `competition_id`,((`g`.`fatness_score_m` + `g`.`fatness_score_f`) / 2) AS `fatness_score`,((`g`.`quality_score_m` + `g`.`quality_score_f`) / 2) AS `quality_score`,((`g`.`taste_score_m` + `g`.`taste_score_f`) / 2) AS `taste_score` from (`rxpb_group_info` `g` left join `rxpb_company_info` `c` on((`g`.`company_id` = `c`.`company_id`)))$$




INSERT INTO `rxpb_user_info` (`user_id`, `user_name`, `password`, `display_name`, `role_id`, `status`, `email`, `create_date`, `create_user`, `update_date`, `update_user`) VALUES (1, 'admin', '21232F297A57A5A743894A0E4A801FC3', 'admin', 1, 1, 'admin@xxx.com', '2017-01-01 00:00:00', 'manual', '2017-01-01 00:00:00', 'manual');

INSERT INTO `rxpb_role_info` (`role_id`, `role_name`, `create_date`, `create_user`, `update_date`, `update_user`) VALUES (1, '管理员', '2017-01-01 00:00:00', 'manual', '2017-01-01 00:00:00', 'manual');

INSERT INTO `rxpb_role_info` (`role_id`, `role_name`, `create_date`, `create_user`, `update_date`, `update_user`) VALUES (2, '评委', '2017-01-01 00:00:00', 'manual', '2017-01-01 00:00:00', 'manual');

INSERT INTO `rxpb_role_info` (`role_id`, `role_name`, `create_date`, `create_user`, `update_date`, `update_user`) VALUES (3, '工作人员', '2017-01-01 00:00:00', 'manual', '2017-01-01 00:00:00', 'manual');


INSERT INTO `rxpb_competition_config` (`id`, `competition_id`, `create_date`, `create_user`, `update_date`, `update_user`) VALUES (1, 1, '2017-01-01 00:00:00', 'manual', '2017-01-01 00:00:00', 'manual');

INSERT INTO `rxpb_competition_info` (`competition_id`, `competition_year`, `var_fatness_m`, `var_fatness_f`, `var_weight_m`, `var_weight_f`, `var_mfatness_sd`, `var_mweight_sd`, `var_ffatness_sd`, `var_fweight_sd`, `result_fatness`, `result_quality`, `result_taste`, `create_date`, `create_user`, `update_date`, `update_user`) VALUES (1, 2017, 0.9, 1, 0.09, 0.1, 0.1, 0.09, 0.1, 0.1, 0, 0, 0, '2017-01-01 00:00:00', 'manual', '2017-01-01 00:00:00', 'manual');
